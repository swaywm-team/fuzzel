Description: fix false-positive use-after-free after failed reallocarray()
 With GCC 14.2.0 on Debian sid, fuzzel fails to build [1] on some non-x86
 archs [2] due to a false-positive use-after-free error for free(tokens)
 or free(tok_lengths), in the case where the corresponding reallocarray()
 call fails. Revise error handling to free() immediately after a failing
 reallocarray(), so that GCC follows that the calls to free() are valid.
 .
 [1] https://buildd.debian.org/status/fetch.php?pkg=fuzzel&arch=riscv64&ver=1.11.1%2Bds-1&stamp=1726441493&raw=0
 [2] loong64, m68k, mips64el, powerpc, ppc64, ppc64el, riscv64, and s390x.
Author: Peter Colberg <peter@colberg.org>
Bug-Debian: https://bugs.debian.org/1085245
Forwarded: https://codeberg.org/dnkl/fuzzel/pulls/437
Last-Update: 2024-10-26
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/match.c
+++ b/match.c
@@ -1455,10 +1455,7 @@ matches_update_internal(struct matches *matches, bool incremental)
 
     if (copy == NULL || tokens == NULL || tok_lengths == NULL) {
         LOG_ERR("failed to allocate tokens");
-        free(copy);
-        free(tokens);
-        free(tok_lengths);
-        goto unlock_and_return;
+        goto free_unlock_and_return;
     }
 
     size_t tok_count = 1;
@@ -1481,24 +1478,16 @@ matches_update_internal(struct matches *matches, bool incremental)
 
             char32_t **new_tokens = reallocarray(
                 tokens, tok_count, sizeof(tokens[0]));
+            if (new_tokens == NULL)
+                goto free_unlock_and_return;
+            tokens = new_tokens;
+
             size_t *new_tok_lengths = reallocarray(
                 tok_lengths, tok_count, sizeof(tok_lengths[0]));
-
-            if (new_tokens == NULL || new_tok_lengths == NULL) {
-                if (new_tokens != NULL)
-                    free(new_tokens);
-                else
-                    free(tokens);
-                if (new_tok_lengths != NULL)
-                    free(new_tok_lengths);
-                else
-                    free(tok_lengths);
-                free(copy);
-                goto unlock_and_return;
-            }
-
-            tokens = new_tokens;
+            if (new_tok_lengths == NULL)
+                goto free_unlock_and_return;
             tok_lengths = new_tok_lengths;
+
             tokens[tok_count - 1] = p + 1;
             tok_lengths[tok_count - 1] = 0;
         }
@@ -1591,10 +1580,6 @@ matches_update_internal(struct matches *matches, bool incremental)
         matches->workers.tok_lengths = NULL;
     }
 
-    free(tok_lengths);
-    free(tokens);
-    free(copy);
-
     LOG_DBG("match update done");
 
     /* Sort */
@@ -1611,6 +1596,11 @@ matches_update_internal(struct matches *matches, bool incremental)
     if (matches->selected >= matches->match_count && matches->selected > 0)
         matches->selected = matches->match_count - 1;
 
+free_unlock_and_return:
+    free(tok_lengths);
+    free(tokens);
+    free(copy);
+
 unlock_and_return:
     matches_unlock(matches);
 }
